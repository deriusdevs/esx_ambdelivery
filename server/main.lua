ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_ambdelivery:unloadMedicines')
AddEventHandler('esx_ambdelivery:unloadMedicines', function()
  
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  local total   = math.random(Config.Salary.Min, Config.Salary.Max)

  TriggerEvent('esx_addonaccount:getSharedAccount', 'society_ambulance', function(account)

  	-- Всем
  	if Config.Salary.Status == 'all' then

    	account.addMoney(total)
    	xPlayer.addMoney(total)
    	TriggerClientEvent('esx:showNotification', source, _U('leave_medicamets_success_total') ..total..'$')

    -- Фракции
    elseif Config.Salary.Status == 'fraction' then

    	account.addMoney(total)
    	TriggerClientEvent('esx:showNotification', source, _U('leave_medicamets_success'))

    -- Игроку
    elseif Config.Salary.Status == 'player' then
    	xPlayer.addMoney(total)
    	TriggerClientEvent('esx:showNotification', source, _U('leave_medicamets_success_total') ..total..'$')

    -- Никому
    else
    	TriggerClientEvent('esx:showNotification', source, _U('leave_medicamets_success'))
    end

  end)

end)