local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData              = {}
local GUI                     = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local Blips                   = {}
local IsWorking               = false
local Point                   = nil
local RouteBlip = {}

ESX                           = nil
GUI.Time                      = 0

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

-- Рандомная точка доставки
function SelectRandomPoint()

  local index = GetRandomIntInRange(1,  #Config.Points)

  for k,v in pairs(Config.ZonesMissions) do
    if v.Pos.x == Config.Points[index].x and v.Pos.y == Config.Points[index].y and v.Pos.z == Config.Points[index].z then
      return k
    end
  end

end

-- Начало миссии
function StartMission()

    -- Определяем рандомную точку 
    Point = SelectRandomPoint()

    -- Навигация
    local v = Config.ZonesMissions[Point]
    RouteBlip['current'] = AddBlipForCoord(v.Pos.x, v.Pos.y, v.Pos.z) 
    SetBlipRoute(RouteBlip['current'], true) 

end

-- Конец миссии
function StopMission()

  Point = nil
  RemoveBlip(RouteBlip['current'])
  RouteBlip['current'] = nil

end

-- Точка миссии
AddEventHandler('esx_ambdelivery:hasEnteredMissionMarker', function(zone)

  if zone == Point then
    ESX.UI.Menu.CloseAll()

    CurrentAction     = 'unload_medicines'
    CurrentActionMsg  = _U('leave_medicamets')
    CurrentActionData = {}
  end

end)

AddEventHandler('esx_ambdelivery:hasExitedMissionMarker', function(zone)
  CurrentAction = nil
end)

-- Показ маркеров
Citizen.CreateThread(function()
  while true do
    Wait(0)
    if PlayerData.job ~= nil and PlayerData.job.name == 'ambulance' then

      local coords = GetEntityCoords(GetPlayerPed(-1))

      if Point ~= nil then
        local v = Config.ZonesMissions[Point]
        if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
        end
      end
   end
  end
end)

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)

    if IsControlJustReleased(1, Keys["DELETE"]) and PlayerData.job ~= nil and PlayerData.job.name == 'ambulance' then
    
    local playerPed = GetPlayerPed(-1)
    
    if IsPedInAnyVehicle(playerPed, false) and IsVehicleModel(GetVehiclePedIsIn(playerPed, false), GetHashKey('ambulance')) then
      if Point == nil then
        -- Начинаем миссию
        StartMission()
      else
        -- Заканчиваем миссию
        StopMission()
      end
    else
      ESX.ShowNotification(_U('spec_auto'))
    end

    end
  end

end)

-- Enter / Exit marker events [ Миссии ]
Citizen.CreateThread(function()
  while true do
    Wait(0)
    if Point ~= nil then
      if PlayerData.job ~= nil and PlayerData.job.name == 'ambulance' then
        local coords      = GetEntityCoords(GetPlayerPed(-1))
        local isInMarker  = false
        local currentZone = nil
        for k,v in pairs(Config.ZonesMissions) do
          if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
            isInMarker  = true
            currentZone = k
          end
        end
        if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
          HasAlreadyEnteredMarker = true
          LastZone                = currentZone
          TriggerEvent('esx_ambdelivery:hasEnteredMissionMarker', currentZone)
        end
        if not isInMarker and HasAlreadyEnteredMarker then
          HasAlreadyEnteredMarker = false
          TriggerEvent('esx_ambdelivery:hasExitedMissionMarker', LastZone)
        end
      end
    end
  end
end)

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)

    if CurrentAction ~= nil then

      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

      if IsControlJustReleased(0, 38) and PlayerData.job ~= nil and PlayerData.job.name == 'ambulance' then

        -- Разгрузка медикаментов
        if CurrentAction == 'unload_medicines' then

            --ESX.ShowNotification(_U('leave_medicamets_success'))

            local playerPed = GetPlayerPed(-1)

            -- Заканчиваем миссию
            StopMission()
            
            TriggerServerEvent('esx_ambdelivery:unloadMedicines')
        end

        CurrentAction = nil
      end
    end
  end
end)

