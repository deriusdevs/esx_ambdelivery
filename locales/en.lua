Locales['en'] = {
  ['start_medicaments_delivery']      = 'Нажмите ~INPUT_CONTEXT~ чтобы начать доставку медикаментов',
  ['leave_medicamets']                = 'Нажмите ~INPUT_CONTEXT~ чтобы доставить медикаменты',
  ['leave_medicamets_success']        = 'Медикаменты ~b~доставлены',
  ['leave_medicamets_success_total']  = 'Медикаменты ~b~доставлены. ~w~Вы заработали: ~g~',
  ['spec_auto']                       = 'Вы должны находиться в ~b~служебном ~w~авто'
}